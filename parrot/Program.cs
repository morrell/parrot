﻿using System.Speech.Synthesis;

namespace parrot
{
    class Program
    {
        static void Main(string[] args)
        {
            SpeechSynthesizer parrot = new SpeechSynthesizer();
            parrot.Speak(string.Join(" ", args));
        }
    }
}
